var SolutionStoreLocatorInit = ( function () {
	let map;
	let infowindow;
	let therapists = [];
	let therapistsInView = [];
	let markers = [];
	let loading = false;

	/**
	 * when a map list item is clicked on re-focus the map with a lat, lon, and zoom
	 *
	 * @param {*} lat
	 * @param {*} lon
	 */
	function centerMap( lat, lon ) {
		map.setCenter({lat: lat, lng: lon});
		map.setZoom(15);
	}

	/**
	 *
	 * @param {*} e
	 */
	function filterTherapistsNames( e ) {
		e.preventDefault;
		let nameFilter = document.querySelector("#name-filter").value.toLowerCase();
		let therapistsInViewBackup = therapistsInView;
		let filteredTherapists = [];

		therapists.forEach(therapist => {
			if (therapist.name.toLowerCase().includes(nameFilter)) {
				filteredTherapists.push(therapist);
			}
		});

		therapistsInView = filteredTherapists;
		updateMap();
		checkFilteredTherapists();
		therapistsInView = therapistsInViewBackup;

		return false; // We're the onsubmit handler
	}

	/**
	 * Setting up autocomplete searchbar for addresses on map
	 */
	function initAutocomplete() {
		// Create the search box and link it to the UI element.
		const input = document.getElementById("pac-input");
		const searchBox = new google.maps.places.SearchBox(input);

		// Bias the SearchBox results towards current map's viewport.
		therapistsInView = therapists;

		map.addListener( "idle", () => { searchBox.setBounds( map.getBounds() ); } );

		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener("places_changed", () => {
			loading = true;
			therapistsInView = [];

			const places = searchBox.getPlaces();

			if (!places.length) {
				loading = false;
				return;
			}

			// Clear out the old markers.
			markers.forEach((marker) => {
				marker.setMap(null);
			});
			markers = [];

			// For each place, get the icon, name and location.
			const bounds = new google.maps.LatLngBounds();
			places.forEach((place) => {
				if (!place.geometry) {
					console.log("Returned place contains no geometry");
					return;
				}

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});

			map.fitBounds(bounds);
			updateMarkers();
			loading = false;
		} );
	}

	/**
	 *
	 */
	function boundsChangedCallback() {
		const bounds = map.getBounds();
		therapistsInView = [];
		for(const t of therapists) {
			if(bounds.contains(new google.maps.LatLng(t.lat, t.lng))) {
				therapistsInView.push(t);
			}
		}
		// It might make sense to sort this by distance prior to updating the map, but we should wait on that
		updateMap();
	};

	/**
	 * we will be getting all Therapist users via a fetch request
	 */
	function getTherapists() {
		const REST_URL = window.location.protocol + "//" + window.location.host + "/wp-json/ssl-map/therapists";
		loading = true;

		fetch( REST_URL, {} )
		.then( async response => {
			const newTherapists = await response.json();

			// check for error response
			if ( !response.ok ) {
				// get error message from body or default to response statusText
				const error = ( newTherapists && newTherapists.message ) || response.statusText;
				return Promise.reject(error);
			}

			newTherapists.forEach( nt => {
				let newTherapist = {
					lat: Number( nt.latitude.trim() ),
					lng: Number( nt.longitude.trim() ),
					name: nt.first_name.trim() + " " + nt.last_name.trim(),
					title: nt.therapist_title.trim(),
					company: nt.therapist_company.trim(),
					street_1: nt.therapist_street_address_1.trim(),
					street_2: nt.therapist_street_address_2.trim(),
					city: nt.therapist_city.trim(),
					state: nt.therapist_state.trim(),
					zip_code: nt.therapist_zip_code.trim(),
					country: nt.therapist_country.trim(),
					phone: nt.therapist_phone.trim(),
					email: nt.therapist_email.trim(),
					website: nt.therapist_website.trim(),
					more_information: nt.therapist_more_information.trim(),
				}
				therapists.push( newTherapist );
			} );
		} )
		.catch( error => {
			console.error( "There is an error- ", error );
		} )
		.finally( () => {
			loading = false;
			boundsChangedCallback();
		} );
	}

	/**
	 * we will be updating our map list and markers when the list of therapists have changed
	 */
	function updateMap() {
		let mapList = document.getElementById("map-list");

		// clearing out current list
		mapList.innerHTML = '';

		if (!therapistsInView.length) {
			checkFilteredTherapists();
		} else {
			markers = therapistsInView.map( ( t ) => {
				const marker = new google.maps.Marker({
					position: { lat: t.lat, lng: t.lng },
				});
				marker.addListener( "click", () => {
					infowindow.close();
					infowindow = getInfoWindow( t );
					infowindow.open( map, marker );
				});

				const mapListItem = getMapListItem( t );
				mapListItem.addEventListener( "click", ( e ) => {
					e.preventDefault();
					centerMap( t.lat, t.lng );
					infowindow.close();
					infowindow = getInfoWindow( t );
					infowindow.open( map, marker );
				} );
				mapList.appendChild( mapListItem );

				return marker;
			});
			renderMarkers();
		}
	}

	/**
	 *
	 * @param {*} t therapist object
	 */
	function getInfoWindow( t ) {
		const address = [];
		t.street_1 && address.push( t.street_1 );
		t.street_2 && address.push( t.street_2 );

		const cityStateZip = [];
		t.city && cityStateZip.push( t.city );
		t.state && cityStateZip.push( t.state );
		t.zip_code && cityStateZip.push( t.zip_code );

		const infoWindowContent = `
			<p>
				<strong>${t.name}${t.title ? `, ${t.title}` : ``}${t.company ? ` | ${t.company}` : ``}</strong>
				${ address.length ? `<br>${address.join(', ')}` : `` }
				${ cityStateZip.length ? `<br>${cityStateZip.join(', ')}` : `` }
				${ t.country ? `<br>${t.country}` : `` }
			</p>
				${ (t.phone || t.email || t.website) ?
					`<p>
						<strong>Contact Information</strong><br>
						${ t.phone ? `<span>Phone: <a href="tel:${t.phone}">${t.phone}</a></span><br>` : `` }
						${ t.email ? `<span>Email: <a href="mailto:${t.email}">${t.email}</a></span><br>` : `` }
						${ t.website ? `<span>Website: <a href="${t.website}" target="_blank">${t.website}</a></span><br>` : `` }
					</p>`
				: ``
			}
			${ t.more_information ? `<p><strong>More Information:</strong><br>${t.more_information.replace(/(?:\r\n|\r|\n)/g, '<br>')}</p>` : `` }
		`;

 		return new google.maps.InfoWindow( {
			content: infoWindowContent,
		} );
	}

	/**
	 *
	 * @param {*} t therapist objects
	 */
	function getMapListItem( t ) {
		const mapListItem = document.createElement( "li" );

		// Build line 2
		let line2 = [];
		if ( t.street_1 ) { line2.push( t.street_1 ); }
		if ( t.street_2 ) { line2.push( t.street_2 ); }
		if ( t.city ) { line2.push( t.city ); }
		if ( t.state ) { line2.push( t.state ); }
		if ( t.zip_code ) { line2.push( t.zip_code ); }
		if ( t.country ) { line2.push( t.country ); }

		mapListItem.innerHTML = `
			<a href=#">
				<div class="container">
					<p class="map-list-item-line1">
						${t.name}${t.title ? `, ${t.title}` : ``}${t.company ? ` | ${t.company}` : ``}
					</p>
					<span class="map-list-item-line2">
						${line2.join( ', ' )}
					    ${t.email ? `| ${t.email}` : ``}
					</span>
				</div>
			</a>
		`;

		return mapListItem;
	}

	/**
	 *
	 */
	function updateMarkers() {
		// we're gonna create a new array for our filtered data, and another new array to hold all of our therapists
		let newTherapists = [];
		let oldTherapists = therapists;
		const mapBounds = map.getBounds();
		for (let i = 0; i < markers.length; i++) {
			const pos = markers[i].getPosition();
			// if the markers are within the bounds of the map and match up with a therapist we will add them to our new array
			if (mapBounds.contains(pos)) {
				if (therapists[i].lat === pos.lat() && therapists[i].lng === pos.lng()) {
					newTherapists.push(therapists[i]);
				}
			}
		}

		// update the map with filtered therapists before resetting
		therapists = newTherapists;
		updateMap();
		therapists = oldTherapists;
	}

	/**
	 * here we're going to check and see if there are any therapists after the filtering happens
	 */
	function checkFilteredTherapists() {
		if(loading) {
			document.querySelector("#map-list").innerHTML = `
				<li class="empty-message">
					<div class="container">
						<p class="fs4">Loading...
							<span class="spinner-grow" role="status">
								<span class="sr-only">Loading...</span>
							</span>
						</p>
					</div>
				</li>
				`;
			return;
		}
		if (!therapists.length) {
		document.querySelector("#map-list").innerHTML = `
			<li class="empty-message">
				<div class="container">
					<p class="fs4">It looks like there are no therapists that match your search.  Please try again.</p>
				</div>
			</li>
			`;
		} else if(!therapistsInView.length && !loading) {
			document.querySelector("#map-list").innerHTML = `
			<li class="empty-message">
				<div class="container">
					<p class="fs4">It looks like there are no therapists in this area.  Please zoom out or pan to an area with therapists.</p>
				</div>
			</li>
			`;
		}
	}

	/**
	 * since the filtering can cause the markers to get a bit wonky, we're gonna re-render every time the map zooms
	 */
	function renderMarkers() {
		const options = {
			imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
			maxZoom: 14,
			averageCenter: true,
		};
		const clusterer = new MarkerClusterer( map, markers, options );
	}

	/**
	 * init function
	 *
	 * This is the only thing exposed in the global scope (as SolutionStoreLocatorInit) and is passed as the callback
	 * parameter to the Google Maps API
	 */
	return function() {
		document.querySelector( '#solution-store-locator-filter' ).onsubmit = ( e ) => {
			e.preventDefault;
			return false;
		};

		map = new google.maps.Map(document.getElementById("map"), {
			zoom: 3,
			center: { lat: 38.5, lng: -98 },
			streetViewControl: false,
			styles: [
				{
					featureType: "poi",
					stylers: [
						{ visibility: "off" }
					]
				}
			]
		});
		infowindow = new google.maps.InfoWindow();

		// Better than bounds_changed! It won't spam, seems to be better than debounce too
		// https://issuetracker.google.com/issues/35818314
		map.addListener( "idle", boundsChangedCallback );
		map.addListener( "idle", renderMarkers );

		getTherapists();
		initAutocomplete();
	}
}() );

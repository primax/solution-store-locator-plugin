<?php
/*
Plugin Name: Solution Store Locator
Plugin URI: #
Description: A Google Maps Store Locator made by Solution Agency. NOTE - You will need to use the JWT Authentication for WP-API plugin by Enrique Chavez for this plugin to work.
Version: 1.0.5
Author: Rob Gruen, Adam Tulloss, Phil Lewis
Author URI: #
License: GPL2
*/

/**
 *
 */
function ssl_map_enqueue() {
    $MAP_API_KEY = 'AIzaSyDL_zf0SnUHwJkxMFaso9m9nP9Zy7z0MaA';
    $MAP_API_CALLBACK = 'SolutionStoreLocatorInit';
    $version = wp_get_theme()->Version;

    if(is_page('find-a-therapist')){
        wp_enqueue_script( 'ssl_map_index', plugin_dir_url( __FILE__ ) . 'js/index.js', [], $version, true );
        wp_enqueue_script( 'ssl_map_markercluster', 'https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js', array(), '', true );
        // this is added in themes functions.php because its on checkout too
        wp_enqueue_script( 'ssl_map_googlemapsapi', "https://maps.googleapis.com/maps/api/js?key={$MAP_API_KEY}&callback={$MAP_API_CALLBACK}&libraries=places&v=weekly", [ 'ssl_map_index' ], '', true );

        wp_enqueue_style( 'ssl_map_css', plugin_dir_url( __FILE__ ) . 'css/style.css', [], $version );
    }
}
add_action('wp_enqueue_scripts', 'ssl_map_enqueue');

/**
 *
 */
function ssl_defer_scripts( $tag, $handle, $src ) {
    $defer = array(
        'ssl_map_googlemapsapi',
    );
    if ( in_array( $handle, $defer ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript" id="' . $handle . '"></script>' . "\n";
    }
    return $tag;
}
add_filter( 'script_loader_tag', 'ssl_defer_scripts', 10, 3 );

/**
 * Shortcode to display the map and list
 */
function ssl_map() {
    ?>
    <div id="ssl_map">
        <div id="map-container">
            <div id="map"></div>
            <div id="map-controls">
                <div id="map-location-search">
                    <form id="solution-store-locator-filter" action="#">
                        <input id="pac-input" class="controls emdr-input" type="text" placeholder="Search by location" />
                    </form>
                </div>
                <ul id="map-list"></ul>
            </div>
        </div>
    </div>
    <?php
}
add_shortcode('ssl-map', 'ssl_map');


/**
 * adds a button below the Latitude and Longitude fields that will calculate the respective coordinates
 */
add_action( 'edit_user_profile','ssl_calc_coordinates', 10, 2 );
add_action( 'show_user_profile','ssl_calc_coordinates', 10, 2 );
function ssl_calc_coordinates($user) {
    wp_enqueue_script( 'google-maps-loader', plugin_dir_url( __FILE__ ) . 'js/googleMaps.js', [], '1.0.0');
?>

    <style>
        tr[data-name=additional_addresses] .acf-actions {
            text-align: left;
        }
    </style>
    <script>
        async function ssl_calc_coordinates() {

            // const { AutocompleteService } = await google.maps.importLibrary('places');
            await google.maps.importLibrary('places');

            if ( typeof acf === 'undefined' ) return;

            // const disabledInputSelector = `.acf-field[data-name="latitude"] input, .acf-field[data-name="longitude"] input, .acf-field[data-name="therapist_city"] input, .acf-field[data-name="therapist_state"] input, .acf-field[data-name="therapist_zip_code"] input, .acf-field[data-name="therapist_country"] input`;
            const disabledInputSelector = `.acf-field[data-name="latitude"] input, .acf-field[data-name="longitude"] input`; // Trusting admins that this will be fine
            const disabledInputs = document.querySelectorAll(disabledInputSelector);
            disabledInputs.forEach(input => {
                // Set tabindex to -1 to prevent tabbing to the input
                input.setAttribute('tabindex', '-1');
                // Set readonly to prevent keyboard input
                input.setAttribute('readonly', 'readonly');
            });

            const get_address_values = ( autocomplete ) => {
                const addressValues = [];
                const place = autocomplete.getPlace();
                const addressComponents = place.address_components;

                    // Get latitude and longitude
                const location = autocomplete.getPlace().geometry?.location;
                if (location) {
                    addressValues.latitude = location.lat();
                    addressValues.longitude = location.lng();
                }

                const targetComponents = ['street_number', 'route', 'locality', 'administrative_area_level_1', 'postal_code', 'country'];
                targetComponents.forEach( component => {
                    addressValues[component] = addressComponents.find( obj => obj.types.includes( component) )?.long_name ?? '';
                } );
                return addressValues;
            }


            const createAutoComplete = (addressInput) => {
                const autocomplete = new google.maps.places.Autocomplete(
                    addressInput,
                    { types: [ 'address' ] },
                );
                autocomplete.setFields( [ 'address_component', 'geometry' ] );

                autocomplete.addListener( 'place_changed', () => {
                    onAutoCompletePlaceChanged( autocomplete, addressInput );
                } );
            }

            const disableNativeAutocomplete = (addressInput) => {
                setTimeout(() => {
                    addressInput.setAttribute("autocomplete", "chrome-off");
                }, 1000);
            }

            const onAutoCompletePlaceChanged = ( autocomplete, addressInput ) => {
                const addressValues = get_address_values( autocomplete );
                const container = addressInput.closest( '.acf-fields' ) || addressInput.closest('.form-table');

                const cityInput = container.querySelector(`[data-name="therapist_city"] input`);
                const stateInput = container.querySelector(`[data-name="therapist_state"] input`);
                const postalCodeInput = container.querySelector(`[data-name="therapist_zip_code"] input`);
                const countryInput = container.querySelector(`[data-name="therapist_country"] input`);
                const latitudeInput = container.querySelector(`[data-name="latitude"] input`);
                const longitudeInput = container.querySelector(`[data-name="longitude"] input`);

                addressInput.value = addressValues['street_number'] + ' ' + addressValues['route'];
                cityInput.value = addressValues['locality'];
                stateInput.value = addressValues['administrative_area_level_1'];
                postalCodeInput.value = addressValues['postal_code'];
                countryInput.value = addressValues['country'];
                latitudeInput.value = addressValues['latitude'] || '';
                longitudeInput.value = addressValues['longitude'] || '';
            }

            document.body.querySelectorAll(`[data-name="therapist_street_address_1"] input`).forEach( addressInput => {
                createAutoComplete( addressInput );
                disableNativeAutocomplete( addressInput );
            } );

            // Additional Addresses, mutation observer to add autocomplete to new address fields as they are added

            const additionalAddresses = document.querySelector( '[data-name="additional_addresses"]' );

            const observer = new MutationObserver( ( mutations ) => {
                const addressInputs = additionalAddresses.querySelectorAll(`[data-name="therapist_street_address_1"] input`);
                // Add custom autocomplete to new address fields, remove default autocomplete
                addressInputs.forEach( addressInput => {
                    addressInput.setAttribute("autocomplete", "off");
                    createAutoComplete( addressInput );
                } );
            } );
            observer.observe( additionalAddresses, { childList: true, subtree: true } );
        }

        window.addEventListener("DOMContentLoaded", function() {
            emdr.LoadGoogleMaps().then(ssl_calc_coordinates);
        });

    </script>
    <br>
<?php }
